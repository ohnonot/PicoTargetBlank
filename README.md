# PicoTargetBlank

## A PicoCMS Plugin to Open External Links in a New Tab

Inspired [by this](https://github.com/picocms/Pico/issues/394#issuecomment-330030163).

Please consult PicoCMS' generic plugin installation instructions.

The plugin is enabled by default.
You can disable it in `config.yml`:

~~~yml
PicoTargetBlank:
    enabled: false
~~~

You can also set the rel= attribute, or disable it with an empty string:

~~~
    rel: 'noopener noreferrer nofollow
or
    rel: ''
~~~

etc.

defaults to `'noopener noreferrer'` if unspecified. See [here for an explanation](https://pointjupiter.com/what-noopener-noreferrer-nofollow-explained/).

## How it works

It picks the rendered page apart and compares each anchor's host to the server's value of `$_SERVER['HTTP_HOST']` - if they differ, it adds `target="_blank"`, the configurable `rel` attribute, a mouseover title and the `external` CSS class to the link.

## CSS

CSS is not included with this plugin. If you want to add a small arrow to the link text, add this to your stylesheets:

~~~css
a.external::after {
   line-height: 100%;
   vertical-align: super;
   font-size: 60%;
   content: "↗"
   }
~~~

There's [no Unicode symbol for the commonly seen box with the outgoing arrow](https://steemit.com/unicode/@markgritter/why-did-unicode-reject-the-external-link-symbol); you can use the above or do some SVG magic.

If you don't want the arrow on _some_ external links, add the "noarrow" class. CSS:

~~~css
a.external.noarrow::after { content: '' }
~~~

---

Source code on [framagit](https://framagit.org/ohnonot/PicoTargetBlank) or [notabug](https://notabug.org/ohnonot/PicoTargetBlank).